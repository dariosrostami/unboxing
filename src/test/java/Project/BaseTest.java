package Project;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.time.Duration;


public class BaseTest {
    public WebDriver driver;

    @Before
    public void setUp() {
        System.out.println("Create drive Firefox");
        driver = new FirefoxDriver();
        System.out.println("Starting login test");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        driver.manage().window().maximize();

    }

    @After
    public void tearDown() {
        System.out.println("Close Driver");
        driver.quit();
    }
}
