package Project;
import Project.utils.Constants;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CreateAccount extends BaseTest{
    @Test
    public void createAnAccount()  {
        driver.get(Constants.homePage +"register");
        WebDriverWait wait = new WebDriverWait(driver, Constants.testingDuration);
        String firstName = RandomStringUtils.randomAlphabetic(7);
        String emailAddress = RandomStringUtils.randomAlphanumeric(11) ;


        driver.findElement(By.xpath("//input[@placeholder='Enter your First Name']")).sendKeys(firstName);
        driver.findElement(By.xpath("//input[@placeholder='Enter your Last Name']")).sendKeys("P"+firstName);
        driver.findElement(By.xpath("//input[@placeholder='Enter your email']")).sendKeys(emailAddress+"@yahoo.com");

        //Enter password and confirm password
        driver.findElement(By.xpath("//input[@placeholder='Enter your password']")).sendKeys("alabala123");
        driver.findElement(By.xpath("//input[@placeholder='Confirm password']")).sendKeys("alabala123");

        //Select country
        WebElement country = driver.findElement(By.xpath("//*[text()='Country']/parent::div/parent::div/parent::div//*[@class=' css-tlfecz-indicatorContainer']"));
        country.click();
        WebElement option = driver.findElement(By.id("react-select-2-option-1"));
        option.click();



        //Push the register button

        WebElement registerButton= wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector("button.bg-emerald-500"))));
        registerButton.click();

        //To verify if the account was created check if the "My account" button appears

        WebElement myAccountButton = wait.until(ExpectedConditions.visibilityOf(
                driver.findElement(By.xpath("//div[@class='hover-icon text-sm bg-emerald-500 p-2 rounded font-bold']"))));

        String accountButton = myAccountButton.getText();
        Assert.assertEquals("My account",accountButton);

    }


}
