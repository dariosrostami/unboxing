package Project;

import Project.utils.Constants;
import org.junit.Assert;
import org.junit.Test;


public class CheckTitle extends BaseTest{

    //Check if clicking the register button takes us to the register page
    @Test
    public void verifyAppTitle(){
        driver.get(Constants.homePage);
        String currentBookingTitle = driver.getTitle();
        String expectedBookingTitle = "Booking App";
        Assert.assertEquals("Wrong page title",expectedBookingTitle, currentBookingTitle);

    }

}

