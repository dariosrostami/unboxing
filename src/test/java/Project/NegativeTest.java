package Project;

import Project.utils.Constants;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class NegativeTest extends BaseTest{
    @Test
    public void loginRandom() throws InterruptedException{
        driver.get(Constants.homePage+"login");
        driver.findElement(By.xpath("//input[@placeholder='Enter your email']")).sendKeys("random@email.com");
        driver.findElement(By.xpath("//input[@placeholder='Enter your password']")).sendKeys("random-password");

        WebElement loginButton = driver.findElement(By.xpath("//button[@class='md:text-lg w-full bg-emerald-500 rounded hover-btn p-2 text-white font-bold']"));
        loginButton.click();


        Assert.assertEquals("The login failed due to incorrect email or password",Constants.homePage,driver.getCurrentUrl());


    }
}
