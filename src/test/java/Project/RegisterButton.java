package Project;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RegisterButton extends BaseTest {
    @Test
    //Check if the register button on the main page takes you to the registration page
    public void checkRegisterButton(){
        driver.get("http://138.68.69.185:3333/");
        WebElement registerButton = driver.findElement(By.xpath("//button[@class='md:text-lg pl-6 pr-6 bg-emerald-500 rounded hover-btn p-2 text-white font-bold whitespace-nowrap']"));
        registerButton.click();
        //Validate URL is correct
        String expectedUrl = "http://138.68.69.185:3333/register";
        Assert.assertEquals(expectedUrl,driver.getCurrentUrl());
        System.out.println("The URL is correct");

    }

}
