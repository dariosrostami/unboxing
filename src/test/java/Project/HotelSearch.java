package Project;

import Project.utils.Constants;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HotelSearch extends BaseTest{
    @Test
    public void IasiHotel(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        //http://138.68.69.185:3333/search?mainInput=iasi
        driver.get(Constants.homePage);
        WebElement searchBar = driver.findElement(By.xpath("//input[@class='outline-none p-2 sm:text-lg w-full']"));
        searchBar.sendKeys("Hotel Iasi");
        driver.findElement(By.cssSelector("button[class=' ml-6 hover-icon text-white p-2 bg-emerald-500 flex items-center justify-center transition ease-in-out rounded-xl duration-500 hover:rounded-full text-2xl']")).click();
        Boolean afterSearch = wait.until(ExpectedConditions.urlToBe("http://138.68.69.185:3333/search?mainInput=Hotel%20Iasi"));
        Assert.assertEquals("http://138.68.69.185:3333/search?mainInput=Hotel%20Iasi",driver.getCurrentUrl());
    }
}
