package Project;
import Project.utils.Constants;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


public class CheckIasiPage extends BaseTest {

    @Test
    public void CheckIasiHotel() {
        driver.get(Constants.homePage);
        driver.findElement(By.xpath("//div[@class='opacity-0 absolute top-0 flex items-end justify-end rounded-xl hover:transition ease-in-out delay-150 w-full h-full semi-transparent-color']")).click();
        String expectedURL ="http://138.68.69.185:3333/hotel-details/4a47d1ea-ce18-4239-bdd8-a9eecfb1b26a";
        Assert.assertEquals(expectedURL,driver.getCurrentUrl());

    }
}
